# Using the IntelliJ IDEA
IntelliJ IDEA is an IDE for Java, but it also understands and provides intelligent coding assistance for a variety of other languages like SQL, HTML, Javascript etc. IntelliJ IDEA is used here because of its integrated version control systems and requirement of no additional plugins to equip it according to the project. IntelliJ IDEA can be used in our project to build the cloned repository. A basic version of IntelliJ IDEA is available for free on their website, this can be downloaded and used for our project. Version 2016.2.4 is used, which would be sufficient for the work to be done. After downloading the application and opening it, an image as shown below appears on the screen.

![](1.JPG)

From the options available below, you can select the import project where it opens a new window.


![Selecting the directory](4.JPG)

Our project contains a pom.xml file and hence you should open it as a maven project. Once the directory is selected, it opens another window as shown below.

![](5.JPG)

Select the Import project from an external model and select the maven option.

![](6.JPG)

The application automatically detects the directory and the type of dependencies in the project. The settings were automatically put to default according to the files. 

![](7.JPG)

For now, you do not need to import any other extra tests to incorporate into the project. In the next step, the application automatically detects relevant SDK, in this the default Java 1.8 JDK was used. The following image shows how IDE takes in the SDK to run the project.

![](9.JPG)

And going through the next steps, you will be able to open the lh-toolkit on the IDE.